# -*- coding: utf-8 -*-
gra=[["DP","D","NP"],
     ["N","book"],
["N","orange"],
["NP","AP","NP"],
["NP","N"],
["AP","A"],
["A","heavy"],
["A","orange"],
["AP","AdvD","AP"],
["D","a"],
["AdvD","very"],
["Coor","and"], 
["NP","NP","Coor","NP"]]

sent=["a", "very", "heavy", "orange", "book", "and", "orange"]


exstate1=("S",["VP"],0,(0,0))
exstate2=("S",["VP"],1,(0,0))
def incomplete(istate):
    """
Function that takes a state of shape (lhs, [rhs, rhs, ...], dot, (from,to)) and returns 
true if complete, false if incomplete
    """ 

    if len(istate[1])==istate[2]:
        return False
    else:
        return True

terminals=set(["book","orange","heavy","a","very","and"])
phraselabels=set([y[0] for y in gra if y[0].endswith("P")])
#categoriallabels=set([v for z in gra for v in z if v not in terminals.union(phraselabels)])

def nextcat(nstate):
    """
    Function that takes a state of form (lhs, [rhs, rhs, ...], dot, (from,to)) and returns
    "term" if it is a terminal, "phase" if it is a phrasal rule, and "cat" for a part of 
    speech rule
    """
    rhs=nstate[1]
    dot=nstate[2]
    if rhs[dot] in terminals:
        return "term"
    elif rhs[dot] in phraselabels:
        return "phrase"
    else:
        return "cat"
    

#function ADD_TO_CHART
def addtochart(somestate,idx):
    """
    Check if entry somestate is already present in chart at index idx, and if not, add it.
    """
    if somestate not in chart[idx]:
        chart[idx].append(somestate)
        print "Added",somestate,"to chart at idx",idx

dummystate=("dummy",["DP"],0,(0,0))

def earley(wor, gramm):
    global chart
    chart={a:list() for a in range(len(wor)+1)} 
    global grammar
    grammar=gramm
    global words
    words=wor
    addtochart(dummystate,0)
    global i
    for i in range(len(words)+1):
        print i
        for state in chart[i]:
            if incomplete(state) and nextcat(state)=="phrase": 
                predictor(state)
            elif incomplete(state) and nextcat(state)=="cat":
                 scanner(state)
            else:
                completer(state)
    return chart

def predictor(predictorstate):
    print "Called predictor"
    next=predictorstate[1][predictorstate[2]]
    predicted=[x for x in grammar if x[0]==next]
    for arule in predicted:
        addtochart((arule[0],arule[1:],0,(predictorstate[3][1],predictorstate[3][1])),predictorstate[3][1])
        
def scanner(scannerstate): 
    print "Called scanner"
    nexts=scannerstate[1][scannerstate[2]]
    j=scannerstate[3][1]
    scanned=[rule for rule in gra if i<len(words) and rule[0]==nexts and words[i]==rule[1]]
    for srule in scanned:
        addtochart((srule[0],srule[1:],1,(j,j+1)),j+1)

def completer(completerstate):
    print "called completer"
    completed=[target for target in chart[completerstate[3][0]] if (target[1][target[2]:]!=[]) and (completerstate[0]==target[1][target[2]:][0]) ]
    for t in completed:
        addtochart((t[0],t[1],t[2]+1,(t[3][0],completerstate[3][1] )),completerstate[3][1])
    
earley(sent,gra)
