# -*- coding: utf-8 -*-
"""
Created on Sun May 11 12:15:54 2014

@author: ulrike
"""

# -*- coding: utf-8 -*-
"""
Created on Sat May  3 11:26:39 2014

@author: ulrike
"""

sentence = "John will see the movie"
sentence=sentence.split()
"""
I formulated the lexical entries as dictionary from Strings to tuples, where tuple[0] is :: or :, and tuple[1] is the stack of features
"""
grammar={"John":("::",["D","-epp"]),"will":("::",["=V","+epp","T"]),"see":("::",["=D","=D","V"]),"the":("::",["=Num","D"]),"movie":("::",["N"])}

emptycat=[("::",["=T","C"]),("::",["=N","Num"])]



def initializechart(sentence1,grammar1,emptycat1):
    """
    Initializes chart as dictionary from tuples to lists of tuples. There are spare entries still (and maybe this step could be done away with). In all entries (x,x):emptycat, and in all entries (1,2) I put words
    """
    chart1 = {(x,y):list() for y in range(len(sentence1)+1) for x in range(len(sentence1)+1)} # Create a dictionary that has tuples of integers as keys and empty lists as values. Do all combinations of integers starting from 0 up to the length of sentence   
    for i in range(len(sentence1)+1):
#       Go through the the cells
        chart1[(i,i)]=(emptycat1)
#        put the empty categories in the cells where the indices are the same
        if i>=1:
#            for all indices greater than 1
            chart1[(i-1,i)].append(grammar1[sentence1[i-1]])
#            Fill up the entries i-1,i eg (0,1) with the word i-1
    return chart1
    
    

def isdiscontinuous(rule):
    """
    checks if a rule involves discontinuous phonetic strings.
    """
    if type({}) in [type(x) for x in rule[1]]:
#        If there is a dictionary in the stack of an entry
        return True
    else:
        return False
        
        
def merge1(ii,kk,jj,rulea,ruleb,chartm1):
    """
    Takes the current indices, two rules and the chart as its argument and merges the two rules. It checks if the thing preceded by the = in rulea is the first thing in the stack part of ruleb, and then rewrites as (:,[remaining a+remaining b]). It adds it to ii, jj in the chart. Returns an empty list if merge is not possible
    """
    if (rulea[1][0][0]=="=")and(rulea[1][0][1:]==ruleb[1][0])and((":",rulea[1][1:]+ruleb[1][1:])not in chartm1[ii,jj]):
        chartm1[ii,jj].append((":",rulea[1][1:]+ruleb[1][1:]))
        return (":",rulea[1][1:]+ruleb[1][1:])
    else:
        return []
        
def mergeempty(ii,kk,jj,chartme):
    """
    Takes the three indices and a chart, and applies all possible empty rules to the three possible cells i,k k,j and i,j, if they are filled. Empty rules are always taken from the left, e.g. i,i k,k and i,i respectively. For example if we are trying it for (4,5), they come from (4,4). Because there could be subsequent applications of empty categories, merging goes on until there are no more changes possible. 
    """
    idxs=[(ii,kk),(kk,jj),(ii,jj)]
    scope=[x for x in idxs if chartme[x]!=[]]
    for aa in scope: 
#for each of the cells that we are considering           
        changed=True
        while changed:
#            repeat his while the variable "changed" is true
            trackchanges=0
#            This variable will increase by one each time we change something to a cell of the chart aa
            for somerule in chartme[aa[0],aa[0]]:
                for anotherrule in chartme[aa]:
                    new=merge1(aa[0],aa[0],aa[1],somerule,anotherrule,chartme)
                    if new !=[]:
                        trackchanges+=1
#                        if merge is successful, increas trackchanges by one
            if trackchanges==0:
#                If there are no more changes, make the loop not continue
                changed=False

def merge3(mergedrule,ii,kk,jj,chartm3):
    """ takes a rule, the current indices and the chart as arguments.
It checks if the next item in the rule is a subcategorizer, if so, it checks the chart higher up and to the left. If it finds something, it applies merge that retains the structure of mergee d within c, by adding a dictionary of the form {(row,col):[feat1 feat2 ...]} to the stack. 
    """
    if mergedrule[1][0].startswith("="):
        n=len(set([x for x,y in chartm3])) 
#        same as len(sentence), but sentence is not a global variable
        selectcells={(row,col):chartm3[(row,col)] for row in range(n+1) for col in range(n+1) if row<ii and col<=jj}
#        Create a new dictionary that has only the things on top and to the left in it. 
        selectrules=[(x,z) for (x,y) in selectcells.items() for z in y]
#       Transform this dictionary into a list of tuples where the first entry is (row,col), and the second entry is something like (:,[a b c]). We cannot loop through the dictionary directly because a dictionary has unique keys
        filterrules=[(d,e) for (d,e) in selectrules if mergedrule[1][0][1:]==e[1][0]]
#       In this list, pick all entries that are subcategorized for in the rule that we apply merge3 to. 
        for coor,arule in filterrules:
#            in filterrules
            for aarule in chartm3[ii,jj]:
#           In the present chart entry try to merge all different combinations
                if (aarule[1][0][0]=="=")and(aarule[1][0][1:]==arule[1][0]):
                    first=aarule[1][1:][0] 
#                   Get the feature stack of the lower rule 
                    stack=[first,{(coor):(":",arule[1][1:])}]
#                   And create a new feature stack with the dictionary in it
                    chartm3[ii,jj].append((":",stack))                    
                    return (":",stack)
    else:
        return []

        
def move(mergedrule,ii,kk,jj,chartmv):
    """
    Takes a rule, the present indices, the chart as argument.
    Checks if a constituent is disconinuous (is there a dictionary in its stack and if so, if there is movement possible, it does the movement
    """
    dinstack=[x for x in mergedrule[1]if type(x)==type({})] 
#    Makes a list of all the dictionaries in the feature stack of an entry
    if dinstack !=[]:
        dinstack=dinstack[0]
#        Picks the first one (there is only one)
    if(isdiscontinuous(mergedrule)
        )and(mergedrule[1][0][0]=="+"
        )and("-"+mergedrule[1][0][1:]==dinstack.values()[0][1][0]):
#   Is there a matching +/- feature pair?
        stackwithoutd=mergedrule[1]
        stackwithoutd.remove(dinstack)
#   Get the stack and remove all dictionaries from it
        chartmv[(dinstack.keys()[0][0],jj)].append((":",stackwithoutd[1:]+dinstack.values()[0][1][1:]))
#        At the starting point of the dictionary in its stack, the end point that the present index provides, append the remaining features of both the dictionary and the rest of the things that is in the stack in the right order. 
        return ((dinstack.keys()[0][0],jj),(":",stackwithoutd[1:]+dinstack.values()[0][1][1:]))
    else:
        return []



def parselefttoright(sent,gram,empty):
    chart=initializechart(sent,gram,empty)
    for j in range(len(sent)+1):
        for i in range(j-2,-1,-1):
            for k in range(i+1,j):
                cell1 = chart[(i,k)] #changed the chart a little bit (sorry)
                cell2 = chart[(k,j)]
                mergeempty(i,k,j,chart)
                for cell1ind,cell1r in enumerate(cell1):
                    for cell2ind,cell2r in enumerate(cell2):
                        new1=merge1(i,k,j,cell1r,cell2r,chart)
                        if new1!=[]:
                            shouldimove1=move(new1,i,k,j,chart)
                            shouldim3=merge3(new1,i,k,j,chart)
                            if shouldim3!=[]:
                                shouldimove2=move(shouldim3,i,k,j,chart)
#                                Basically: try merge1 first, then move, then merge3, then move again. Maybe ultimately mergeempty should apply between those steps. If you find that, add mergeempty(i,k,j,chart) whereever you need it. 
            mergeempty(i,k,j,chart)
    return chart

def parserighttoleft(sent,gram,empty):
    chart=initializechart(sent,gram,empty)
    for j in range(len(sent),-1,-1):
        for i in range(j-2,-1,-1):
            for k in range(i+1,j):
                cell1 = chart[(i,k)] #changed the chart a little bit (sorry)
                cell2 = chart[(k,j)]
                mergeempty(i,k,j,chart)
                for cell1ind,cell1r in enumerate(cell1):
                    for cell2ind,cell2r in enumerate(cell2):
                        new1=merge1(i,k,j,cell1r,cell2r,chart)
                        if new1!=[]:
                            shouldimove1=move(new1,i,k,j,chart)
                            shouldim3=merge3(new1,i,k,j,chart)
                            if shouldim3!=[]:
                                shouldimove2=move(shouldim3,i,k,j,chart)
            mergeempty(i,k,j,chart)
    return chart



parselefttoright(sentence,grammar,emptycat)
parserighttoleft(sentence,grammar,emptycat)

#==============================================================================
#VERBOSE VERSION
#==============================================================================
def vinitializechart(sentence1,grammar1,emptycat1):
    """
    Initializes chart as dictionary from tuples to lists of tuples. There are spare entries still (and maybe this step could be done away with). In all entries (x,x):emptycat, and in all entries (1,2) I put words
    """
    chart1 = {(x,y):list() for y in range(len(sentence1)+1) for x in range(len(sentence1)+1)}
    for i in range(len(sentence1)+1):
        chart1[(i,i)]=(emptycat1)
        if i>=1:
            chart1[(i-1,i)].append(grammar1[sentence1[i-1]])
            print "chart initialized with empty categories",emptycat,"and dimensions",len(sentence)+1,len(sentence)+1
    return chart1
    
    
#def whichrule

def visdiscontinuous(rule):
    """
    checks if a rule involves discontinuous phonetic strings.
    """
    if type({}) in [type(x) for x in rule[1]]:
        return True
    else:
        return False
        
        
def vmerge1(ii,kk,jj,rulea,ruleb,chartm1):
    """
    Takes two rules as its argument and merges them. It checks if the thing preceded by the = is the first thing in the stack part of b, and then rewrites as (:,[remaining a+remaining b]). It adds it to ii, jj in the chart. 
    """
    if (rulea[1][0][0]=="=")and(rulea[1][0][1:]==ruleb[1][0])and((":",rulea[1][1:]+ruleb[1][1:])not in chartm1[ii,jj]):
        print "Merge1",rulea,ruleb,"\n-->(:",rulea[1][1:]+ruleb[1][1:],")"
        chartm1[ii,jj].append((":",rulea[1][1:]+ruleb[1][1:]))
        print "M1 ADDED ENTRY\n",(":",rulea[1][1:]+ruleb[1][1:]),"TO",(ii,jj)
        return (":",rulea[1][1:]+ruleb[1][1:])
    else:
        return []
        
#def mergeempty(aa,bb,chartm):
def vmergeempty(ii,kk,jj,chartme):
    """
    Takes two tuples of indices and a chart, and applies all possible empty rules to the second entry. Empty rules are always taken from the left, e.g. if we are trying it for (4,5), they come from (4,4)
    """
    idxs=[(ii,kk),(kk,jj),(ii,jj)]
    scope=[x for x in idxs if chartme[x]!=[]]
    print "These are the cells that I am trying to mergeeempty with:\n",scope
    for aa in scope:            
        changed=True
        while changed:
            trackchanges=0
            for somerule in chartme[aa[0],aa[0]]:
                for anotherrule in chartme[aa]:
                    new=vmerge1(aa[0],aa[0],aa[1],somerule,anotherrule,chartme)
                    if new !=[]:
                        print "Mergeempty",(aa[0],aa[0]),somerule, (aa),anotherrule,"\n-->",new
                        print "ME ADDED",new,"TO CHART AT",aa
                        trackchanges+=1
            if trackchanges==0:
                changed=False

#def merge3(c,d,coor):
def vmerge3(mergedrule,ii,kk,jj,chartm3):
    """ takes two rules and a tuple of indices, and applies merge that retains the structure of mergee d within c. This returns a rule with a dictionary in its stack. 
    """
    if mergedrule[1][0].startswith("="):
        n=len(set([x for x,y in chartm3])) 
        selectcells={(row,col):chartm3[(row,col)] for row in range(n+1) for col in range(n+1) if row<ii and col<=jj}
        print "selectcells",selectcells
        selectrules=[(x,z) for (x,y) in selectcells.items() for z in y]
        print "selectrules",selectrules
        filterrules=[(d,e) for (d,e) in selectrules if mergedrule[1][0][1:]==e[1][0]]
        if filterrules!=[]:
            print "filterrules",filterrules
        for coor,arule in filterrules:
            for aarule in chartm3[ii,jj]:
                if (aarule[1][0][0]=="=")and(aarule[1][0][1:]==arule[1][0]):
                    first=aarule[1][1:][0] #is the last 0 ok?
                    stack=[first,{(coor):(":",arule[1][1:])}]
                    print "merge3",aarule,arule,"-->(\":\",",stack,")"
                    chartm3[ii,jj].append((":",stack))                    
                    print "M3 ADDED ENTRY",(":",stack),"TO CHART AT",(ii,jj)
                    return (":",stack)
    else:
        return []

        
#def move(h,chart4,idxr,idxc):
def vmove(mergedrule,ii,kk,jj,chartmv):
    """
    Checks if a constituent is disconinuous (is there a dictionary in its stack(needs to be changed to tuple)) and if so, if there is movement possible, it does the movement
    """
    dinstack=[x for x in mergedrule[1]if type(x)==type({})] #can there only be one? across the board movement?
    if dinstack !=[]:
        print "Started vmove"
        dinstack=dinstack[0]
        print "Localized this dictionary in the stack\n",dinstack
    if(isdiscontinuous(mergedrule)
        )and(mergedrule[1][0][0]=="+"
        )and("-"+mergedrule[1][0][1:]==dinstack.values()[0][1][0]):
        stackwithoutd=mergedrule[1]
        stackwithoutd.remove(dinstack)
        chartmv[(dinstack.keys()[0][0],jj)].append((":",stackwithoutd[1:]+dinstack.values()[0][1][1:]))
        print "MV ADDED",(":",stackwithoutd[1:]+dinstack.values()[0][1][1:]),"TO CHART AT",(dinstack.keys()[0][0],jj)
        return ((dinstack.keys()[0][0],jj),(":",stackwithoutd[1:]+dinstack.values()[0][1][1:]))
    else:
        return []



def vparselefttoright(sent,gram,empty):
    chart=initializechart(sent,gram,empty)
    for j in range(len(sent)+1):
        for i in range(j-2,-1,-1):
            for k in range(i+1,j):
                cell1 = chart[(i,k)] 
                cell2 = chart[(k,j)]
                print "checking cells",(i,k),(k,j) 
                vmergeempty(i,k,j,chart)
                for cell1ind,cell1r in enumerate(cell1):
                    for cell2ind,cell2r in enumerate(cell2):
                        print "trying to merge (i,k),cell1r,(k,j),cell2r",(i,k),cell1r,(k,j),cell2r
                        new1=vmerge1(i,k,j,cell1r,cell2r,chart)
                        if new1!=[]:
                            shouldimove1=vmove(new1,i,k,j,chart)
                            shouldim3=vmerge3(new1,i,k,j,chart)
                            print "shouldimove1",shouldimove1
                            if shouldim3!=[]:
                                shouldimove2=vmove(shouldim3,i,k,j,chart)
            vmergeempty(i,k,j,chart)
    return chart

def vparserighttoleft(sent,gram,empty):
    chart=initializechart(sent,gram,empty)
    for j in range(len(sent),-1,-1):
        for i in range(j-2,-1,-1):
            for k in range(i+1,j):
                cell1 = chart[(i,k)] 
                cell2 = chart[(k,j)]
                print "checking cells",(i,k),(k,j) 
                vmergeempty(i,k,j,chart)
                for cell1ind,cell1r in enumerate(cell1):
                    for cell2ind,cell2r in enumerate(cell2):
                        print "trying to merge (i,k),cell1r,(k,j),cell2r",(i,k),cell1r,(k,j),cell2r
                        new1=vmerge1(i,k,j,cell1r,cell2r,chart)
                        if new1!=[]:
                            shouldimove1=vmove(new1,i,k,j,chart)
                            shouldim3=vmerge3(new1,i,k,j,chart)
                            print "shouldimove1",shouldimove1
                            if shouldim3!=[]:
                                shouldimove2=vmove(shouldim3,i,k,j,chart)
            vmergeempty(i,k,j,chart)
    return chart



vparselefttoright(sentence,grammar,emptycat)
vparserighttoleft(sentence,grammar,emptycat)
