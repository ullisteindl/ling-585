map={  'A':{"B":0,"C":1},
            'B':{"D":2,"E":3},
            "C":{"D":4,"E":5},
            "D":{"F":6},
            "E":{"F":7},
            "F":{}}
            
start = "A"
end = "F"
distances = {node:"NA" for node in map} 
distances[end]=0

def V(node): 
    print "node",node
    print "map[node]",map[node]
    if distances[node] == "NA": 
        print "node,distances[node]", node, distances[node]
        vdict = {V(dest)+map[node][dest]: dest for dest in map[node]}
        print "vdict",vdict
        best = vdict[min(vdict)]
        print "best",best
        distances[node]=min(vdict)
        print "distances[node]", distances[node]
    return distances[node] 
        
pathlen= V(start) 
print "The fastest path from "+start+" to "+end+" is "+str(pathmap)+", which is "+str(pathlen)+" long"

